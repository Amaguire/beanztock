package com.citi.training.polling;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class ScheduledTask {
	//@Scheduled(fixedDelay = 5000)
	public void scheduleFixedDelayTask() {
	    System.out.println(
	      "Fixed delay task - " + System.currentTimeMillis() / 1000);
	    //OnlineFeed onlineFeed = new OnlineFeed();
	    //onlineFeed.getGoogle("goog");
	    //onlineFeed.getMicro();
	}
}