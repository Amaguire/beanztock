package com.citi.training.polling;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class OnlineFeed {

	public static int count =0;
	
	public String getComp(String comp) {
		RestTemplate restTemplate = new RestTemplate();
		String url  = "http://feed.conygre.com:8080/MockYahoo/quotes.csv?s=" + comp + "&f=p0";
		count++;
		ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
		System.out.println(response.getBody());
		return response.getBody();
	}
	
//	public void getMicro() {
//		RestTemplate restTemplate = new RestTemplate();
//		String url  = "http://feed.conygre.com:8080/MockYahoo/quotes.csv?s=MSFT&f=p0";
//		ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
//		System.out.println(response.getBody());
//	}
}