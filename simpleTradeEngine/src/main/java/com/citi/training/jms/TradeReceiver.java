package com.citi.training.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class TradeReceiver {

	@JmsListener(destination = "OrderBroker_Reply")
	public void receive(Message message) throws JMSException {
		System.out.println(message.getJMSCorrelationID());
		System.out.println(((TextMessage) message).getText());
		
	}
}