package com.citi.training.jms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.citi.training.data.entities.Trade;

@Component
public class TradeSender {

//	<trade>
//	<buy>true</buy> <!-- so, false for a sale -->
//	<id>0</id>
//	<price>88.0</price>
//	<size>2000</size>
//	<stock>HON</stock>
//	<whenAsDate>2014-07-31T22:33:22.801-04:00</whenAsDate>
//	</trade>
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
//	public void sendSimple() {
//		String messageToSend = "	<trade>\r\n" + 
//				"	<buy>true</buy> \r\n" + 
//				"	<id>1</id>\r\n" + 
//				"	<price>88.0</price>\r\n" + 
//				"	<size>2000</size>\r\n" + 
//				"	<stock>HON</stock>\r\n" + 
//				"	<whenAsDate>2014-07-31T22:33:22.801-04:00</whenAsDate>\r\n" + 
//				"	</trade>";
//		System.out.print("Sending: " + messageToSend);
//		jmsTemplate.convertAndSend("OrderBroker", messageToSend,
//				message -> {
//					message.setJMSCorrelationID("1");
//					return message;
//				});
//	}
	
	
	//need to set up trade to xml and getID
	public void sendTrade(Trade trade) {
		String messageToSend = trade.toXml(trade);
		System.out.print("Sending: " + messageToSend);
		jmsTemplate.convertAndSend("OrderBroker", messageToSend,
				message -> {
					message.setJMSCorrelationID(trade.getId().toString());
					return message;
				});
	}
	
	
}