package com.citi.training.engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.TradeEngineApplication;
import com.citi.training.data.entities.Markets;
import com.citi.training.data.entities.Strategies;
import com.citi.training.data.entities.Trade;
import com.citi.training.data.repository.JdbcStratRepository;
import com.citi.training.data.services.MarketService;
import com.citi.training.data.services.StratService;
import com.citi.training.data.services.TradeService;
import com.citi.training.jms.TradeReceiver;
import com.citi.training.jms.TradeSender;
import com.citi.training.polling.OnlineFeed;

@Component
public class TradeEngine {

    private static final Logger log = LoggerFactory.getLogger(TradeEngine.class);

    @Autowired
    private TradeService tradeService;
    
    @Autowired
    private TradeSender tradesender;

    @Autowired
    private TradeReceiver tradeReceiver;
    
    @Autowired
    JdbcStratRepository stratRepo;
    
    @Autowired
    StratService stratSer;
    
    @Autowired
    MarketService markSer;
    
    @Autowired
    OnlineFeed onlineFeed;
  
    @Scheduled(fixedRate = 1000)
    public void sendTrades() {
    	//int countLong = 0;
    	double longPrice = 0;
    	//get list of companys
    	//polling
    	
    	List<Markets> markList = new ArrayList<Markets>(markSer.getAllMarkets());
    	//renamed from findallmarkets
    	List<String> comps = new ArrayList<String>();
    	//populate comp
    	for(Markets m : markList) {
    		comps.add(m.getCompanyName());
    	}
    	
    	HashMap<String, Double> longMap = new HashMap<String, Double>();
    	for (String i : comps) {
    		longMap.put(i.toString(),0.0);
    	}
    	//populate price
    	//int countLong = 0;
    	//double number = 0;
    	for(String i : comps) {
    		double number = longMap.get(i);
    		String price = onlineFeed.getComp(i);
    		price = price.replace("\n", "");
    		number += Double.parseDouble(price);
    		longMap.replace(i, number/OnlineFeed.count);
    	}
    	
    	List<Strategies> strat = new ArrayList<Strategies>(stratSer.findAll());
    	Trade createTrade = new Trade();
    	Boolean enterLoop = true;
    	while(OnlineFeed.count > 3 && enterLoop) {
    		
    	
    	for (Strategies s : strat) {
    		log.info(s.getName());
    		createTrade.setStock(s.getName());
    		createTrade.setState(Trade.TradeState.INIT);
    		createTrade.setStrat_id(s.getStrat_id());
    		createTrade.setSize(s.getQuantity());
    		System.out.print(s.getName());
    		String price = onlineFeed.getComp(s.getName());
    		price = price.replace("\n", "");
    		double numbers = Double.parseDouble(price);
    		
    		
    		String livePrice = onlineFeed.getComp(s.getName());
    		livePrice = livePrice.replace("\n", "");
    		double liveVal = Double.parseDouble(price);
    		
    	    createTrade.setPrice(liveVal);
    		
    		if(longPrice < liveVal) {
    			//algo will decide to buy or sell
        		createTrade.setTradeType(Trade.TradeType.valueOf("SELL"));
        		tradeService.saveTrade(createTrade);
    	    	tradesender.sendTrade(createTrade);
    	    	enterLoop = false;
    		} else {
    			createTrade.setTradeType(Trade.TradeType.valueOf("BUY"));
    			tradeService.saveTrade(createTrade);
    	    	tradesender.sendTrade(createTrade);
    	    	enterLoop = false;
    		}
  
    	}
    	}
    	
    	//buy or sell
    	//wri
        List<Trade> initTrades = tradeService.getTradesByState(Trade.TradeState.INIT);
        log.debug("Processing " + initTrades.size() + " new trades");
        for(Trade trade : initTrades) {     
        	//calling method
        	//returns buy or sell
        	//TradeType tradeType=RunStrategy(trade)
        	//trade.setTradeType(tradeType);
            trade.stateChange(Trade.TradeState.WAITING_FOR_REPLY);            
            tradeService.saveTrade(trade);
            tradesender.sendTrade(trade);
            //algo for trades?
            
//            trade.stateChange(Trade.TradeState.FILLED);          
//            tradeService.saveTrade(trade);
//            tradesender.sendTrade(trade);
        }      
    }   
}