package com.citi.training.data.entities;

public class Strategies {

	private int strat_id;
	private String name;
	private String shortTime;
	private String longTime;
	private double aveShortPrice;
	private double aveLongPrice;
	private double lowestPrive;
	private double highestPrice;
	private double buyThres;
	private double sellThres;
	private String exitTimeFrame;
	private int active;
	private int quantity;
	private int company_id;
	private String companyName;
	
	
	
	public Strategies(int strat_id, String name, String shortTime, String longTime, double aveShortPrice,
			double aveLongPrice, double lowestPrive, double highestPrice, double buyThres, double sellThres,
			String exitTimeFrame, int active, int quantity, int company_id, String companyName) {
		super();
		this.strat_id = strat_id;
		this.name = name;
		this.shortTime = shortTime;
		this.longTime = longTime;
		this.aveShortPrice = aveShortPrice;
		this.aveLongPrice = aveLongPrice;
		this.lowestPrive = lowestPrive;
		this.highestPrice = highestPrice;
		this.buyThres = buyThres;
		this.sellThres = sellThres;
		this.exitTimeFrame = exitTimeFrame;
		this.active = active;
		this.quantity = quantity;
		this.company_id = company_id;
		this.companyName = companyName;
	}


	public int getCompany_id() {
		return company_id;
	}


	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}


	public String getCompanyName() {
		return companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public Strategies(int strat_id, String name, String shortTime, String longTime, double aveShortPrice,
			double aveLongPrice, double lowestPrive, double highestPrice, double buyThres, double sellThres,
			String exitTimeFrame, int active, int quantity, int companyid) {
		super();
		this.strat_id = strat_id;
		this.name = name;
		this.shortTime = shortTime;
		this.longTime = longTime;
		this.aveShortPrice = aveShortPrice;
		this.aveLongPrice = aveLongPrice;
		this.lowestPrive = lowestPrive;
		this.highestPrice = highestPrice;
		this.buyThres = buyThres;
		this.sellThres = sellThres;
		this.exitTimeFrame = exitTimeFrame;
		this.active = active;
		this.quantity = quantity;
		this.company_id = companyid;
	}


	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public Strategies() {
		
	}
	
	public Strategies(int stratId) {
		super();
		this.strat_id = stratId;
	}

	
	


	public int getStrat_id() {
		return strat_id;
	}
	public void setStrat_id(int strat_id) {
		this.strat_id = strat_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getShortTime() {
		return shortTime;
	}
	public void setShortTime(String shortTime) {
		this.shortTime = shortTime;
	}
	public String getLongTime() {
		return longTime;
	}
	public void setLongTime(String longTime) {
		this.longTime = longTime;
	}
	public double getAveShortPrice() {
		return aveShortPrice;
	}
	public void setAveShortPrice(double aveShortPrice) {
		this.aveShortPrice = aveShortPrice;
	}
	public double getAveLongPrice() {
		return aveLongPrice;
	}
	public void setAveLongPrice(double aveLongPrice) {
		this.aveLongPrice = aveLongPrice;
	}
	public double getLowestPrive() {
		return lowestPrive;
	}
	public void setLowestPrive(double lowestPrive) {
		this.lowestPrive = lowestPrive;
	}
	public double getHighestPrice() {
		return highestPrice;
	}
	public void setHighestPrice(double highestPrice) {
		this.highestPrice = highestPrice;
	}
	public double getBuyThres() {
		return buyThres;
	}
	public void setBuyThres(double buyThres) {
		this.buyThres = buyThres;
	}
	public double getSellThres() {
		return sellThres;
	}
	public void setSellThres(double sellThres) {
		this.sellThres = sellThres;
	}
	public String getExitTimeFrame() {
		return exitTimeFrame;
	}
	public void setExitTimeFrame(String exitTimeFrame) {
		this.exitTimeFrame = exitTimeFrame;
	}
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getCompanyid() {
		return company_id;
	}

	public void setCompanyid(int companyid) {
		this.company_id = companyid;
	}
}