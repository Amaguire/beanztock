package com.citi.training.data.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.data.entities.Strategies;


@Component
public class JdbcStratRepository {
	private static final Logger logger = LogManager.getLogger(JdbcStratRepository.class);
	
	@Autowired
	private JdbcTemplate tpl;
	
	public List<Strategies> findAll(){
		
		logger.info("Find all invoked");
		return tpl.query("Select * from strategies INNER JOIN markets on strategies.company_id=markets.company_id",
				new StratMapper());
	}
	
	public Strategies findById(int id) {
		return tpl.queryForObject("Select * from strategies where strat_id=?",
				new Object[] {id},
				new StratMapper());
	}
	
	public int save(Strategies strat) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
        this.tpl.update(
            new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps =
                    connection.prepareStatement("insert into strategies(name, shortTime, longTime, aveShortPrice, aveLongPrice, lowestPrice, highestPrice,buyThres ,sellThres, exitTimeFrame, active, quantity, company_id) "
                    		+ "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)", Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, strat.getName());
                    ps.setString(2, strat.getShortTime());
                    ps.setString(3, strat.getLongTime());
                    ps.setDouble(4, strat.getAveShortPrice());
                    ps.setDouble(5, strat.getAveLongPrice());
                    ps.setDouble(6, strat.getLowestPrive());
                    ps.setDouble(7, strat.getHighestPrice());
                    ps.setDouble(8, strat.getBuyThres());
                    ps.setDouble(9, strat.getSellThres());
                    ps.setString(10, strat.getExitTimeFrame());
                    ps.setInt(11, strat.getActive());
                    ps.setInt(12, strat.getQuantity());
                    ps.setInt(13, strat.getCompanyid());
                    return ps;
                }
            },
            keyHolder);
        //return keyHolder.getKey().intValue();
        return 0;
	}
	
	private static final class StratMapper implements RowMapper<Strategies> {
	    public Strategies mapRow(ResultSet rs, int rowNum) throws SQLException {
	    	Strategies strat = new Strategies(rs.getInt("strat_id"),rs.getString("name"),
	        		rs.getString("shortTime"),rs.getString("longTime"),rs.getDouble("aveShortPrice"),
	        		rs.getDouble("aveLongPrice"), rs.getDouble("lowestPrice"), rs.getDouble("highestPrice"), rs.getDouble("buyThres"),
	        		rs.getDouble("sellThres"), rs.getString("exitTimeFrame"), rs.getInt("active"), rs.getInt("quantity"), rs.getInt("company_id"),rs.getString("company"));	        
			return strat;
	    }
	}
	
}
