package com.citi.training.data.entities;

public class Markets {
	private int company_id;
	private String company;
	
	public Markets() {
		
	}
	
	public Markets(int marketId, String companyName) {
		super();
		this.company_id = marketId;
		this.company = companyName;
	}

	public int getMarketId() {
		return company_id;
	}

	public void setMarketId(int marketId) {
		this.company_id = marketId;
	}

	public String getCompanyName() {
		return company;
	}

	public void setCompanyName(String companyName) {
		this.company = companyName;
	}
	
	
	
}
