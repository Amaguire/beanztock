package com.citi.training.data.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.data.entities.Markets;
import com.citi.training.data.entities.Strategies;
import com.citi.training.data.repository.JdbcStratRepository;

@Component
public class StratService {

	@Autowired
	JdbcStratRepository jdbcStratRepo;
	
	public List<Strategies> findAll(){
		return jdbcStratRepo.findAll();
	}
	
	public int save(Strategies strat) throws IllegalAccessException {
		if(strat.getName()==null) {
			throw new IllegalAccessException();
		}
		return jdbcStratRepo.save(strat);
	}
	
	public Strategies findById(int id) throws Exception {
		if(id < 1){
			throw new Exception();
		}
		return jdbcStratRepo.findById(id);
	}
	
}
