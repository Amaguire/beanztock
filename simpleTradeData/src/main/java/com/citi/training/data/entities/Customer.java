package com.citi.training.data.entities;

public class Customer {

	private int custID;
	private String custName;
	private double balance;
	private int shares;
	
	public Customer() {
		
	}
	
	public Customer(int custID, String custName, double balance, int shares) {
		super();
		this.custID = custID;
		this.custName = custName;
		this.balance = balance;
		this.shares = shares;
	}
	public int getCustID() {
		return custID;
	}
	public void setCustID(int custID) {
		this.custID = custID;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public int getShares() {
		return shares;
	}
	public void setShares(int shares) {
		this.shares = shares;
	}
}
