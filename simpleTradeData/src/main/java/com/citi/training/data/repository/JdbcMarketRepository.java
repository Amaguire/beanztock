package com.citi.training.data.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.citi.training.data.entities.Markets;
import com.citi.training.data.entities.Strategies;
import com.citi.training.data.entities.Trade;

@Repository("JdbcMarketRepository")
public class JdbcMarketRepository {

	private static final Logger log = LoggerFactory.getLogger(JdbcMarketRepository.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	private JdbcTemplate tpl;
	
	public List<Markets> getAllMarkets(){
		log.debug("JdbcMarketRepo getAll");
		List<Markets> markets = jdbcTemplate.query("select * from markets",
				new MarketMapper());
		//MarketMapperNeeded
		log.debug("Query for all returned list of size: " + markets.size());
        return markets;
	}
	
	public int save(Markets markets) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
        this.tpl.update(
            new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps =
                    connection.prepareStatement("insert into strategies(company) "
                    		+ "values (?)", Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, markets.getCompanyName());
              
                    return ps;
                }
            },
            keyHolder);
        //return keyHolder.getKey().intValue();
        return 0;
	}
	
	public Markets findbyId (int id) {
        return tpl.queryForObject
        		("Select * from markets "
        				+ "where company_id=?", 
        				new Object[]{id},
        				new MarketMapper());
    }
	
	private static final class MarketMapper implements RowMapper<Markets> {
	    public Markets mapRow(ResultSet rs, int rowNum) throws SQLException {
	        Markets markets = new Markets(rs.getInt("company_id"),
	        		rs.getString("company"));	        
			return markets;
	    }
	}
	
	public int rowCount() {
        return tpl.queryForObject
        		("select count(*) from markets",
        				Integer.class);
    }
	

	
	
	
}
