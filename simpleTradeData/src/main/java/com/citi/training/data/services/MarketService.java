package com.citi.training.data.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.citi.training.data.entities.Markets;
import com.citi.training.data.entities.Trade;
import com.citi.training.data.repository.JdbcMarketRepository;

@Service

public class MarketService {
	
    @Autowired
    private JdbcMarketRepository jdbcMarketRepository;
	
    public List<Markets> getAllMarkets() {
        return jdbcMarketRepository.getAllMarkets();
    }
    
    public int rowCount()
    {
        return jdbcMarketRepository.rowCount();
       
    }
   
    public Markets findbyId (int id) {
        if (id < 1)
        {
            throw new IllegalArgumentException();
        }  
        return jdbcMarketRepository.findbyId(id)    ;  
    }
	
	public int save(Markets markets) throws IllegalAccessException {
		if(markets.getCompanyName()==null) {
			throw new IllegalAccessException();
		}
		return jdbcMarketRepository.save(markets);
	}
	
}
