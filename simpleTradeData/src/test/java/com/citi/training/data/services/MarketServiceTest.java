package com.citi.training.data.services;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.data.entities.Markets;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MarketServiceTest {

	@Autowired
	MarketService marketService;
	
	@Test(expected=IllegalArgumentException.class)
	public void MarketService_InvalidIdNumber_ReturnsIllegalArgExc() {
		marketService.findbyId(0);
	}
	
	@Test()
	public void marketService_ValidId_ReturnsMarket() {
			Markets market=marketService.findbyId(1);
			assertThat(market, is(not(nullValue())));		
	}

	@Test()
	public void marketService_ListAll_ReturnsListMarkets() {
			List<Markets> market=marketService.getAllMarkets();
			assertTrue(market.size()>0);		
	}
	
//	@Test
//	public void marketService_SaveMarket_ReturnsMarket() throws IllegalAccessException {
//		//todo general test
//		int result=marketService.save(new Markets(0,"ORNG"));
//		Markets market = marketService.findbyId(result);		
//		assertTrue(market.getCompanyName().equals("ORNG"));
//	}
}