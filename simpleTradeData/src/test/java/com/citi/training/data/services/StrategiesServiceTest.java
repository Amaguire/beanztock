package com.citi.training.data.services;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.data.entities.Strategies;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StrategiesServiceTest {

	@Autowired
	StratService stratService;
	
//	@Test(expected=IllegalArgumentException.class)
//	public void strategiesService_InvalidIdNumber_ReturnsIllegalArgExc() throws Exception {
//		stratService.findById(0);
//	}
	
//	@Test()
//	public void strategiesService_ValidId_ReturnsStrategies() throws Exception {
//			Strategies strat=stratService.findById(2);
//			assertThat(strat, is(not(nullValue())));		
//	}

	@Test()
	public void strategiesService_ListAll_ReturnsListStrategies() {
			List<Strategies> strat=stratService.findAll();
			assertTrue(strat.size()>0);		
	}
	
//	@Test
//	public void strategiesService_SaveStrategies_ReturnsStrategies() throws Exception {
//		//todo general test
//		
//		int stratId = 0;
//		String name = "TEST3";
//		String shortTime = "2";
//		String longTime = "6";
//		double aveShortPrice = 5;
//		double aveLongPrice = 5;
//		double lowestPrive = 2;
//		double highestPrice = 60;
//		double buyThres = 10;
//		double sellThres = 8;
//		String exitTimeFrame = "4";
//		int active = 1;
//		int quantity = 1;
//		int company_id =1;
//		
//		int result=stratService.save(new Strategies(stratId, name, shortTime, longTime, aveShortPrice, aveLongPrice,
//				lowestPrive, highestPrice, buyThres, sellThres, exitTimeFrame,
//				active, quantity, company_id));
//		
//		
//		Strategies strat = stratService.findById(result);		
//		assertTrue(strat.getName().equals("TEST3"));
//	}
}