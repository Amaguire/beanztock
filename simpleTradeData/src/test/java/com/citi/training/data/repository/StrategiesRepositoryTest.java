package com.citi.training.data.repository;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.data.entities.Strategies;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StrategiesRepositoryTest {
	@Autowired
	JdbcStratRepository jdbcStratRepository;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void strategiesRepo_CheckRowCount_ReturnsOver0() {
	
		//assertTrue(jdbcStratRepository.rowCount()>0);
		
	}

	@Ignore
	@Test
	public void strategiesRepo_CheckFindById_ReturnsStrategyName() {
		//Strategies strategies=jdbcStratRepository.findbyId(2);
		//assertTrue(strategies.getName().equals("BOOM"));
		
	}
	
	@Test
	public void strategiesRepo_CheckFindAll_ReturnsStrategiesRecords() {
		List<Strategies> strategies=jdbcStratRepository.findAll();
		
		assertTrue(strategies.size()>0);
		
	}
	
	@Test
	public void strategiesRepo_SaveStrategies_ReturnsStrategies() {
		int stratId = 0;
		String name = "TEST2";
		String shortTime = "2";
		String longTime = "6";
		double aveShortPrice = 5;
		double aveLongPrice = 5;
		double lowestPrive = 2;
		double highestPrice = 60;
		double buyThres = 10;
		double sellThres = 8;
		String exitTimeFrame = "4";
		int active = 1;
		int quantity = 1;
		int company_id = 1;
		
		int result=jdbcStratRepository.save(new Strategies(stratId, name, shortTime, longTime, aveShortPrice, aveLongPrice,
				lowestPrive, highestPrice, buyThres, sellThres, exitTimeFrame,
				active, quantity, company_id));
		
		int result1=jdbcStratRepository.save(new Strategies(stratId, name, shortTime, longTime, aveShortPrice, aveLongPrice,
				lowestPrive, highestPrice, buyThres, sellThres, exitTimeFrame,
				active, quantity, company_id));
		
//		Strategies strategies = jdbcStratRepository.findbyId(result);
//		Strategies strategies1 = jdbcStratRepository.findbyId(result1);
		
//		strategies.setStratId(6);
//		strategies1.setName(name);
//		strategies1.setShortTime(shortTime);
//		strategies1.setLongTime(longTime);
//		strategies1.setAveShortPrice(aveShortPrice);
//		strategies1.setAveLongPrice(aveLongPrice);
//		strategies1.setLowestPrive(lowestPrive);
//		strategies1.setHighestPrice(highestPrice);
//		strategies1.setBuyThres(buyThres);
//		strategies1.setSellThres(sellThres);
//		strategies1.setExitTimeFrame(exitTimeFrame);
//		
//		assertTrue(strategies.getName().equals(name));
//		assertTrue(strategies.getShortTime().equals(shortTime));
//		assertTrue(strategies.getLongTime().equals(longTime));
//		assertTrue(strategies.getAveShortPrice()==aveShortPrice);
//		assertTrue(strategies.getAveLongPrice()==aveLongPrice);
//		assertTrue(strategies.getLowestPrive()==lowestPrive);
//		assertTrue(strategies.getHighestPrice()==highestPrice);
//		assertTrue(strategies.getBuyThres()==buyThres);
//		assertTrue(strategies.getSellThres()==sellThres);
//		assertTrue(strategies.getExitTimeFrame().equals(exitTimeFrame));
//		
//		assertTrue(strategies1.getName().equals(name));
//		assertTrue(strategies1.getShortTime().equals(shortTime));
//		assertTrue(strategies1.getLongTime().equals(longTime));
//		assertTrue(strategies1.getAveShortPrice()==aveShortPrice);
//		assertTrue(strategies1.getAveLongPrice()==aveLongPrice);
//		assertTrue(strategies1.getLowestPrive()==lowestPrive);
//		assertTrue(strategies1.getHighestPrice()==highestPrice);
//		assertTrue(strategies1.getBuyThres()==buyThres);
//		assertTrue(strategies1.getSellThres()==sellThres);
//		assertTrue(strategies1.getExitTimeFrame().equals(exitTimeFrame));
//		assertTrue(strategies1.getStratId()!=strategies.getStratId());
		//delete record after
		
	}

}