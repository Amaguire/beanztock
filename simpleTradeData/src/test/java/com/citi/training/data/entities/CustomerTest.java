package com.citi.training.data.entities;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void customer_CanInstantiate_ReturnsNotNull() {
		
		int custID = 0;
		String custName = "John";
		double balance = 200.0;
		int shares = 40;
		
		Customer cust = new Customer(custID, custName, balance, shares);
		
		assertThat(new Customer(custID, custName, balance, shares), is(not(nullValue())));

		cust.setCustID(4);
		cust.setCustName("James");
		cust.setBalance(250.0);
		cust.setShares(50);
		
		assertTrue(cust.getCustName().equals("James"));
		assertTrue(cust.getCustID()==4);
		assertTrue(cust.getBalance()==250.0);
		assertTrue(cust.getShares()==50);
	}

}