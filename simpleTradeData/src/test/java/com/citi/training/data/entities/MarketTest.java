package com.citi.training.data.entities;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MarketTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void market_CanInstantiate_ReturnsNotNull() {
		assertThat(new Markets(1,"ORNG"), is(not(nullValue())));

	}

}