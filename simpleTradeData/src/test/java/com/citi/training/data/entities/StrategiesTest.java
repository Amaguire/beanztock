package com.citi.training.data.entities;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StrategiesTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void strategies_CanInstantiate_ReturnsNotNull() {
		
		int strat_id = 1;
		String name = "TEST1";
		String shortTime = "1";
		String longTime = "5";
		double aveShortPrice = 12;
		double aveLongPrice = 12;
		double lowestPrive = 3;
		double highestPrice = 85;
		double buyThres = 15;
		double sellThres = 12;
		String exitTimeFrame = "6";
		int active = 1;
		int quantity = 1;
		int company_id = 1;
		
		assertThat(new Strategies(strat_id, name, shortTime, longTime, aveShortPrice, aveLongPrice,
				lowestPrive, highestPrice, buyThres, sellThres, exitTimeFrame,
				active, quantity, company_id), is(not(nullValue())));

	}

}