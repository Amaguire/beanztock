package com.citi.training.data.repository;

import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.data.entities.Markets;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MarketRepositoryTest {
	@Autowired
	JdbcMarketRepository marketRepository;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

//	@Test
//	public void marketRepo_CheckRowCount_ReturnsOver0() {
//	
//		assertTrue(marketRepository.rowCount()>0);
//		
//	}

//	@Test
//	public void marketRepo_CheckFindById_ReturnsCompanyName() {
//		Markets market=marketRepository.findbyId(1);
//		assertTrue(market.getCompanyName().equals("AAPL"));
//		
//	}
	
	@Test
	public void marketRepo_CheckFindAll_ReturnsMarketRecords() {
		List<Markets> market=marketRepository.getAllMarkets();
		
		assertTrue(market.size()>0);
		
	}
	
	@Ignore
	@Test
	public void marketRepo_SaveMarket_ReturnsMarket() {
		int id = 0;
		String name = "ORNG";
		
		int result=marketRepository.save(new Markets(id,name));
		int result1=marketRepository.save(new Markets(id,name));
		
//		Markets market = JdbcMarketRepository.findbyId(result);	
//		Markets market1 = JdbcMarketRepository.findbyId(result1);
		
//		market1.setCompany("ORNG1");
//		market1.setCompany_id(8);
		
//		assertTrue(market.getCompany().equals("ORNG"));
//		assertTrue(market1.getCompany().equals("ORNG1"));
//		assertTrue(market1.getCompany_id()==8);
//		assertTrue(market.getCompany_id()!=market1.getCompany_id());
		//delete record after
		
	}

}