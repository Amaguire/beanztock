
$(document).ready(startup);

function startup(){
	
	
	getCompIdTrade();
	
	$("#buttonSave").click(saveTrade);
	$("#dialogSaved").dialog({
		modal : true,
		autoOpen : false,
		buttons : {
			Ok : function() {
				$(this).dialog("close");
			}
		}
	});

}

function getCompIdTrade(){
	
	$.ajax({
		

	url : "/markets",
	// data : params,
	dataType : "json",
	type : "GET",
	cache : false,
	async : false,
	contentType : 'application/json; charset=utf-8',
	success : function(response) {
		var output= "<td>Company:</td>" +"<td><select id='company' name='company'>";
	
						      
		for (var i = 0; i<response.length; i++) {
		output += " <option value=" + response[i].companyName + " >" + response[i].companyName + "</option>"
			}
		output += "</select></td>";
		
		$("#strategytrade").html(output);
	
		
		
	},
	error : function(err) {
		alert("Error: " + err.responseText)
		
	}
		
	});
}
	

function saveTrade() {

	var params = {
		tradeType : $("#tradeType").val(),
		stock : $("#strategytrade").val(),
		lastStateChange : "2018-01-20 09:24:09",
		state : "INIT",
		strat_id : "1",
		price: "25",
		size:$("#size").val(),

	};

	var paramsJson = JSON.stringify(params);
	alert(paramsJson);
	$.ajax({
		url : "/trades/save",
		data : paramsJson,
		dataType : "json",
		type : "POST",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {
			$("#dialogSaved").dialog('open');
			// alert(response);

		},
		error : function(err) {
			alert("Error: " + err.responseText)
		}
	});
	}