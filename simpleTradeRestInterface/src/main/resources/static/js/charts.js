google.charts.load('current', {'packages':['line']});
      google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

      var data = new google.visualization.DataTable();
      data.addColumn('number', 'AAPL');
      data.addColumn('number', 'GOOG');
      data.addColumn('number', 'BRK-A');
      data.addColumn('number', 'NSC');
      data.addColumn('number', 'MSFT');
      
      data.addRows([
        [1,  37.8, 20.8, 41.8, 5],
        [2,  30.9, 21.5, 32.4, 6],
        [3,  25.4,   37, 25.7, 87],
        [4,  11.7, 18.8, 10.5, 53],
        [5,  11.9, 17.6, 10.4, 63],
        [6,   8.8, 13.6,  7.7, 23],
        [7,   7.6, 12.3,  9.6, 52],
        [8,  12.3, 29.2, 10.6, 31],
        [9,  16.9, 42.9, 14.8, 24],
        [10, 12.8, 30.9, 11.6, 24],
        [11,  75.3,  47.9,  4.7,64],
        [12,  76.6,  58.4,  5.2,66],
        [13,  84.8,  66.3,  3.6,65],
        [14,  84.2,  76.2,  3.4, 54]
      ]);

      var options = {
        chart: {
          title: 'Trades Per Strategy and Company'
        },
       
      };

      var chart = new google.charts.Line(document.getElementById('linechart_material'));

      chart.draw(data, google.charts.Line.convertOptions(options));
    }