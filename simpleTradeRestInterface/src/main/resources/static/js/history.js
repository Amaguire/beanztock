

$(document).ready(startup);

function startup() {
	

	getCompNameHis();
	//getCompNameTradeHistory();
	
	
	$('.tabCompanyHis').click(function(){
		var strId= (event.target.id);
		//alert(strId);
		var result = strId.split("_");
		var v_marketid=result[1];
		getTradeHistory(v_marketid);
		//getTradeByComp(v_marketid)});
	});
	
	$("#modalLoginForm").dialog({
		autoOpen : false,
		height : 650,
		width : 450,
		modal : true,
		buttons : {

			Cancel : function() {
				$("#modalLoginForm").dialog("close");
			}
		},
		close : function() {

			$("#modalLoginForm").dialog("close");
		}
	});

}

function getCompId() {

	$.ajax({

		url : "/markets",
		// data : params,
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {
			var output = "<tr><td>Company:</td>"+
			"<td><select id='companylist' name='company' style='margin-bottom: 5px'>";

			for (var i = 0; i<response.length; i++) {
				output += " <option value=" + response[i].marketId + " >" + response[i].companyName + "</option>"
					}
				output += "</select></td></tr>";
				
			
			$("#stratComp").html(output);

			$("#modalLoginForm").dialog("open");

		},
		error : function(err) {
			alert("Error: " + err.responseText)

		}

	});
}
function getCompNameHis() {

	$
			.ajax({

				url : "/markets",
				// data : params,
				dataType : "json",
				type : "GET",
				cache : false,
				async : false,
				contentType : 'application/json; charset=utf-8',
				success : function(response) {

					var outputName = "<ul class='nav nav-tabs'><li class='active'><a data-toggle='tab' href='#tradeHistory'></a></li>";
					for (var i = 0; i < response.length; i++) {
						outputName += " <li><td><a id='tab_"  + response[i].marketId+ "' class='tabCompanyHis' data-toggle='tab' href='#tabContentHis"
								+ response[i].marketId + "'>"
								+ response[i].companyName + "</a></li>"
								
					}
					outputName += "</ul>";
				
					
					
					var outputContent = "<div class='tab-contentHis'>"
						for (var i = 0; i < response.length; i++) {
							outputContent+= "<div id='tabContentHis" 
								+ response[i].marketId +"' class='tab-pane fade in active'></div>";
						}
					outputContent+="</div>";
					
					
					
					$("#tradeHistory").html(outputName + outputContent);

				},
				error : function(err) {
					alert("Error: " + err.responseText)

				}

			});
}

	function getCompNameTradeHistory() {

	$
			.ajax({

				url : "/markets",
				
				// data : params,
				dataType : "json",
				type : "GET",
				cache : false,
				async : false,
				contentType : 'application/json; charset=utf-8',
				success : function(response) {

					var outputName = "<ul class='nav nav-tabs'><li class='active'><a data-toggle='tab' href='#tradeHistory'>TradeHistory</a></li>";
					for (var i = 0; i < response.length; i++) {
						outputName += " <li><td><a data-toggle='tab' href="
								+ response[i].companyName + ">"
								+ response[i].companyName + "</a></li>";
						
					}
					outputName += "</ul>";
					
					
					$("#tradeHistory").html(outputName);
						
					

				},
				error : function(err) {
					alert("Error: " + err.responseText)

				}

			});
}



	
	function getTradeHistory(p_marketid) {

		
		
		$.ajax({

					url : "trades/strat/" + p_marketid,
					// data : params,
					dataType : "json",
					type : "GET",
					cache : false,
					async : false,
					contentType : 'application/json; charset=utf-8',
					success : function(response) {

						var outputName = "";//"<ul class='nav nav-tabs'><li class='active'><a data-toggle='tab' href='#sharesheld2'>Shares Held</a></li>";
						for (var i = 0; i < response.length; i++) {
							outputName += " <table class='table'>" +
									"<tr><th> Trade Number </th> <th>Stock Name :</th><th>Price:</th><th>Quantity of Stock</th><th>Trade Type</th><th>Last Updated</th></tr>"
								+   "<tr><td>"+ response[i].id + "</td>"
								+ 	"<td>"+ response[i].stock + "</td>" 
								+ 	"<td>"+ response[i].price + "</td>" 
								+ 	"<td>"+ response[i].size+ "</td>" 
								+ 	"<td>"+ response[i].tradeType+ "</td>"
								+ 	"<td>"+ response[i].lastStateChange+ "</td></tr>"
						}
						outputName += "</table>";
						
						
						$("#tabContentHis" + p_marketid).html(outputName);

					},
					error : function(err) {
						alert("Error: " + err.responseText)

					}

				});
	}





