package com.citi.training.rest.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.data.entities.Markets;
import com.citi.training.data.entities.Strategies;
import com.citi.training.data.entities.Trade;
import com.citi.training.data.services.MarketService;

@RestController
@RequestMapping("/markets")
public class MarketRestController {
	
	public static final Logger log = LoggerFactory.getLogger(
			MarketRestController.class);
	
	@Autowired
	private MarketService marketService;
	
	
	@RequestMapping(method = RequestMethod.GET,
			produces = {"application/json"})
	@ResponseStatus(HttpStatus.CREATED)
	public List<Markets> getAll(){
		return marketService.getAllMarkets();
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public int save(@RequestBody Markets markets) throws IllegalAccessException {
		return marketService.save(markets);
	}
	
    @RequestMapping(value = "/findbyid",
            method = RequestMethod.GET)
    public Markets findbyId(int id)
    {
        return marketService.findbyId(id)   ;
    }
	
    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String getStatus()
    {
               
        return "Market Controller running";
    }
    
}
