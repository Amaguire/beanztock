package com.citi.training.rest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.data.entities.Markets;
import com.citi.training.data.entities.Strategies;
import com.citi.training.data.services.StratService;

@RestController
@RequestMapping("/strats")
public class StratRestController {

	@Autowired
	StratService stratService;
	
	@RequestMapping(method = RequestMethod.GET)		
	public List<Strategies> getListOfCustomers() {
		return stratService.findAll();
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public int save(@RequestBody Strategies strat) throws IllegalAccessException {
		return stratService.save(strat);
	}
	
	
	
}
