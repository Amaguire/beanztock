$(document).ready(startup);

function startup(){
	getAllStrats();
	
	$("#buttonSave2").click(saveStrategy);
	
	$("#dialogSaved").dialog({
		modal : true,
		autoOpen : false,
		buttons : {
			Ok : function() {
				$(this).dialog("close");
			}
		}
	});
}


function saveStrategy() {
	/*{
		"stratId": 1,
		"name": "Test2",
		"shortTime": "1",
		"longTime": "4",
		"aveShortPrice": 10,
		"aveLongPrice": 10,
		"lowestPrive": 1,
		"highestPrice": 100,
		"buyThres": 20,
		"sellThres": 15,
		"exitTimeFrame": "2",
		"active": 1
		}*/
	
	
	
	var e = document.getElementById("companylist");
	var strCompany = e.options[e.selectedIndex].value;
	//alert ( strCompany);
	var strCompanyname = e.options[e.selectedIndex].text;
	//strCompanynamealert ( strCompanyn);

	//var a =document.getElementById("name");
	//var strName = a.options[a.option:selected].value;

	
	//var compName = $( "#myselect option:selected" ).text();
	
	
	var params = {
		stratId: -1,
		name: strCompanyname,
		shortTime: $("#shortTime").val(),
		longTime:$("#longTime").val(),
		aveShortPrice: 0,
		aveLongPrice: 0,
		lowestPrive: 0,
		highestPrice: 0,
		buyThres: 0,
		sellThres: 0,
		exitTimeFrame: 0,
		active: 1,
		quantity:$("#quantity").val(),
		companyid:strCompany

	};

	var paramsJson = JSON.stringify(params);
	alert(paramsJson);
	$.ajax({
		url : "/strats/save",
		data : paramsJson,
		dataType : "json",
		type : "POST",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {
			$("#dialogSaved").dialog('open');
			// alert(response);

		},
		error : function(err) {
			alert("Error: " + err.responseText)
		}
	});
	}


function getAllStrats() {

	$.ajax({

		url : "/strats",
		// data : params,
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',
		success : function(response) {
			var output = "";
			

			for (var i = 0; i<response.length; i++) {
				
				output +=" <div class='panel-group' id='accordion'>"+
				"<div class='panel panel-default'>"+
					"<div class='panel-heading'>";
				
				
				output += "<td><h4 class='panel-title'><a data-toggle='collapse' data-parent='#accordion' href='#"+response[i].strat_id+"'>" + response[i].name +
				"</a></h4></div>";
				
				
				output +=" <td><div id='"+response[i].strat_id+"' class='panel-collapse collapse'><div  class='panel-body'> Company Name : "+response[i].companyName +"";
				
				output +=" <p > Quantity : "+response[i].quantity +"</p>";
				output +=" <p> Short Time  : "+response[i].shortTime + "</p>";
				output +=" <p> Long Time : "+response[i].longTime+"</p>";
				output +=" <p> Average Short Price : "+response[i].aveShortPrice +"</p>";
				output +=" <p> Average Long Price : "+response[i].aveLongPrice +"</p>";
				output +=" <p >Lowest Price : "+response[i].lowestPrice +"</p>";
				output +=" <p> Highest Price : "+response[i].highestPrice +"</p>";
				output +=" <p> Buy Threshold : "+response[i].buyThres +"</p>";
				output +=" <p> Sell Threshold : "+response[i].sellThres +"</p>";
				output +=" <p>Exit Time Frame : "+response[i].exitTimeFrame +"</p>";
				 
				
			
				output += "</div></div></div></td>"
					}
			
				output +="";
			
				
			
			$("#StratList").html(output);


		},
		error : function(err) {
			alert("Error: " + err.responseText)

		}

	});
}







